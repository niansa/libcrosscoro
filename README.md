# libcrosscoro C++20 cross-platform coroutine library
Based on https://github.com/jbaldwin/libcoro but cross-platform

## Use in your CMake project

    cmake_minimum_required(VERSION 3.11)
    
    # Fetch the project and make it available for use.
    include(FetchContent)
    FetchContent_Declare(
        libcrosscoro
        GIT_REPOSITORY https://gitlab.com/niansa/libcrosscoro.git
        GIT_TAG        <TAG_OR_GIT_HASH>
    )
    FetchContent_MakeAvailable(libcrosscoro)
    
    # Link the libcoro cmake target to your project(s).
    target_link_libraries(${PROJECT_NAME} PUBLIC crosscoro)
